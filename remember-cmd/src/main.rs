// use copypasta::{ClipboardContext, ClipboardProvider};
use copypasta_ext::prelude::*;
use copypasta_ext::try_context;
use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode, KeyEventKind},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use dirs::home_dir;
use std::collections::HashSet;
use std::env;
use std::error::Error;
use std::fs::read_to_string;
use std::fs::{File, OpenOptions};
use std::io::prelude::*;
use std::io::Error as IoError;
use std::io::ErrorKind;
use std::path::Path;
use tui::backend::Backend;
use tui::layout::{Constraint, Layout};
use tui::style::Color;
use tui::style::{Modifier, Style};
use tui::widgets::*;
use tui::widgets::{Table, TableState};
use tui::Frame;
use tui::{backend::CrosstermBackend, Terminal};

enum AppStop {
    Refresh(bool),
    Edit(bool),
    Exit(String),
}

struct DBEntry {
    name: String,
    command: String,
}

impl DBEntry {
    fn new(name: String, command: String) -> Result<DBEntry, Box<dyn Error>> {
        match name.is_empty() || command.is_empty() {
            true => Err(Box::new(IoError::new(
                ErrorKind::InvalidInput,
                "Name or command is empty",
            ))),
            false => Ok(DBEntry { name, command }),
        }
    }
    fn write(&self, file_name: &str) -> Result<(), Box<dyn Error>> {
        // Check if name already exists in database
        if DBEntry::check_in_db(&self.name, file_name)? {
            return Err(Box::new(IoError::new(
                ErrorKind::InvalidInput,
                "Name already exists in database",
            )));
        }

        // Create and write to the file
        let stored_string = format!("{} {}", self.name, self.command);
        let mut file = OpenOptions::new()
            .write(true)
            .create(true)
            .append(true)
            .open(file_name)?;

        // Write the data to the file
        file.write_all(stored_string.as_bytes())?;
        file.write_all("\n".as_bytes())?;

        Ok(())
    }
    fn check_in_db(name: &str, file_name: &str) -> Result<bool, Box<dyn Error>> {
        let contents = read_from_db(file_name);
        for i in &contents {
            if i[0] == name {
                return Ok(true);
            }
        }
        Ok(false)
    }
}

#[derive(Clone)]
struct App {
    state: TableState,
    items: Vec<Vec<String>>,
    running: bool,
    selected_command: String,
    file_path: String,
    refresh: bool,
    edit: bool,
}

impl App {
    fn new() -> Result<App, Box<dyn Error>> {
        // find home directory of the user
        let home = home_dir().unwrap();
        let file_path = format!("{}/.remember-cmd", home.display());
        // check if file exists
        if !Path::new(&file_path).exists() {
            File::create(&file_path)?;
        }
        Ok(App {
            state: TableState::default(),
            items: read_from_db(&file_path),
            running: true,
            selected_command: String::new(),
            file_path: String::from(&file_path),
            refresh: false,
            edit: false,
        })
    }

    pub fn next(&mut self) {
        let i = match self.state.selected() {
            Some(i) => {
                if i >= self.items.len() - 1 {
                    0
                } else {
                    i + 1
                }
            }
            None => 0,
        };
        self.state.select(Some(i));
    }

    pub fn previous(&mut self) {
        let i = match self.state.selected() {
            Some(i) => {
                if i == 0 {
                    self.items.len() - 1
                } else {
                    i - 1
                }
            }
            None => 0,
        };
        self.state.select(Some(i));
    }

    pub fn done(&mut self) {
        if self.items.len() >= 1 {
            self.selected_command = self.items[self.state.selected().unwrap()][1].to_string();
        }
        self.running = false
    }

    pub fn delete(&mut self) {
        if (&self.items).len() >= 1 {
            self.selected_command = self.items[self.state.selected().unwrap()][0].to_string();
            // find line containing selected command and remove it
            let mut new_items: Vec<Vec<String>> = vec![];
            self.items.clone().into_iter().for_each(|i| {
                if i[0] != self.selected_command {
                    new_items.push(i.clone());
                }
            });
            // clear all contents of the file
            let mut file = OpenOptions::new()
                .write(true)
                .create(true)
                .truncate(true)
                .open(self.file_path.clone())
                .unwrap();
            file.write("".as_bytes()).unwrap();
            new_items.into_iter().for_each(|i| {
                write_to_db(
                    DBEntry::new(i[0].clone(), i[1].clone()).unwrap(),
                    &self.file_path.as_str(),
                )
                .unwrap();
            });
            self.refresh = true;
            self.running = false
        } else {
            self.running = false
        }
    }

    pub fn edit(&mut self) {
        self.edit = true;
        self.running = false;
    }

    pub fn search(&mut self) {
        // TODO: Implement a search thingy with fzf, similar to accepting an input to create a new
        // entry
        unimplemented!()
    }
}

fn get_user_input() -> Result<DBEntry, Box<dyn Error>> {
    let stdin = std::io::stdin();
    let mut iterator = stdin.lock().lines();
    let mut name = String::new();
    let mut command = String::new();
    println!("Enter name: ");
    let line1 = iterator.next().unwrap().unwrap();
    let words: Vec<&str> = line1.trim().split_whitespace().collect();
    if words.len() != 1 {
        return Err(Box::new(IoError::new(
            ErrorKind::InvalidInput,
            format!(
                "Only 1 word allowed for the name of a command, got {}",
                words.len()
            ),
        )));
    } else {
        name = words[0].to_string();
    }
    println!("Enter command: ");
    let line2 = iterator.next().unwrap().unwrap();
    if line2.trim().is_empty() {
        return Err(Box::new(IoError::new(
            ErrorKind::InvalidInput,
            "Command cannot be empty",
        )));
    }
    command = line2.trim().to_string();
    println!("name: {}\ncommand: {}", &name, &command);
    let entry = DBEntry::new(name, command)?;
    Ok(entry)
}

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    let app = App::new().unwrap();
    let path = app.file_path.clone();

    if args.len() == 2 {
        match args[1].as_str() {
            "write" => {
                let entry = get_user_input()?;
                write_to_db(entry, &path)?;
            }
            _ => {
                return Err(Box::new(IoError::new(
                    ErrorKind::InvalidInput,
                    "Invalid argument",
                )))
            }
        }
    }
    if args.len() == 1 {
        // setup terminal
        enable_raw_mode()?;
        let mut stdout = std::io::stdout();
        execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
        let backend = CrosstermBackend::new(stdout);
        let mut terminal = Terminal::new(backend)?;

        // create app and run it
        let res = run_app(&mut terminal, app);

        // restore terminal
        disable_raw_mode()?;
        execute!(
            terminal.backend_mut(),
            LeaveAlternateScreen,
            DisableMouseCapture,
        )?;
        terminal.show_cursor()?;
        terminal.clear()?;

        match res {
            Ok(AppStop::Refresh(refresh)) => {
                if refresh {
                    main()?
                }
            }
            Ok(AppStop::Edit(edit)) => {
                if edit {
                    let entry = get_user_input()?;
                    write_to_db(entry, &path)?;
                    main()?
                }
            }
            Ok(AppStop::Exit(command)) => {
                if command.is_empty() {
                    return Ok(());
                }
                println!("Command: {}", &command);
                let ctx = try_context();
                match ctx {
                    Some(mut ctx) => {
                        ctx.set_contents(command.clone()).unwrap();
                    }
                    None => {
                        println!("No clipboard found");
                    }
                }
            }
            Err(err) => {
                println!("{err:?}");
            }
        }
        return Ok(());
    }
    Ok(())
}

fn run_app<B: Backend>(terminal: &mut Terminal<B>, mut app: App) -> std::io::Result<AppStop> {
    while app.running {
        terminal.draw(|f| ui(f, &mut app))?;

        if let Event::Key(key) = event::read()? {
            if key.kind == KeyEventKind::Press {
                match key.code {
                    KeyCode::Char('q') => return Ok(AppStop::Exit("".to_string())),
                    KeyCode::Down => app.next(),
                    KeyCode::Up => app.previous(),
                    KeyCode::Char('j') => app.next(),
                    KeyCode::Char('k') => app.previous(),
                    KeyCode::Char('d') => app.delete(),
                    KeyCode::Char('i') => app.edit(),
                    // KeyCode::Char('/') => app.search(),
                    KeyCode::Enter => app.done(),
                    _ => {}
                }
            }
        }
    }
    if app.refresh {
        return Ok(AppStop::Refresh(true));
    }
    if app.edit {
        return Ok(AppStop::Edit(true));
    }
    Ok(AppStop::Exit(app.selected_command))
}

fn read_from_db(file_name: &str) -> Vec<Vec<String>> {
    // [[title, command], [title, command]]
    // config like this:
    // title command
    // title command
    let contents: Vec<String> = read_to_string(file_name)
        .unwrap()
        .lines()
        .map(String::from)
        .collect();
    let mut formatted_vec = Vec::new();
    for i in &contents {
        let line: Vec<&str> = i.split_whitespace().collect::<Vec<&str>>();
        let command: String = line[1..].join(" ");
        formatted_vec.push(vec![line[0].to_string(), command.clone()]);
    }
    // find duplidate names
    let mut seen = HashSet::new();
    formatted_vec.retain(|x| seen.insert(x[0].clone()));
    // sort by name
    formatted_vec.sort_by(|a, b| a[0].cmp(&b[0]));
    formatted_vec.to_vec()
}

fn write_to_db(entry: DBEntry, file_name: &str) -> Result<(), Box<dyn Error>> {
    match entry.write(file_name) {
        Ok(_) => {}
        Err(err) => {
            println!("{err:?}");
        }
    }
    Ok(())
}

fn ui<B: Backend>(f: &mut Frame<B>, app: &mut App) {
    let rects = Layout::default()
        .constraints([Constraint::Percentage(100)].as_ref())
        .split(f.size());

    let selected_style = Style::default()
        .add_modifier(Modifier::REVERSED)
        .add_modifier(Modifier::BOLD);
    let normal_style = Style::default().bg(Color::Blue);
    let header_cells = ["Name", "Command"].iter().map(|h| {
        Cell::from(*h).style(
            Style::default()
                .fg(Color::Black)
                .add_modifier(Modifier::BOLD),
        )
    });
    let header = Row::new(header_cells)
        .style(normal_style)
        .height(1)
        .bottom_margin(1);
    let rows = app.items.iter().map(|item| {
        let height = item
            .iter()
            .map(|content| content.chars().filter(|c| *c == '\n').count())
            .max()
            .unwrap_or(0)
            + 1;
        let cells = item.iter().map(|c| Cell::from(c.to_owned()));
        Row::new(cells).height(height as u16).bottom_margin(1)
    });
    let t = Table::new(rows)
        .header(header)
        .block(Block::default().borders(Borders::ALL).title("remember-cmd"))
        .highlight_style(selected_style)
        .highlight_symbol(">> ")
        .widths(&[
            Constraint::Percentage(50),
            Constraint::Max(30),
            Constraint::Min(10),
        ]);
    f.render_stateful_widget(t, rects[0], &mut app.state);
}
