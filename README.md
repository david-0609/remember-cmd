# remember-cmd

A simple command line tool to "bookmark" your favourite commands that you never remember

## Usage

- `j`, `k`, and arrow keys to navigate up and down the table
- `i` to insert a new entry
- `d` to delete entry under the cursor
- `$ remember-cmd write` to add a new entry without opening the TUI

The "database" is stored at `~/.remember-cmd`

## Installation

### Prerequisites
- `cargo` installed on your system
- **Note that this plugin is only tested to work on Linux X11 and Wayland**

1. `cd` into `./remember-cmd`
2. `cargo install --path`
